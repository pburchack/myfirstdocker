import asyncio
import logging
import sys
import os

sys.path.append(os.getcwd())

from bot.src.echobot import main

logging.basicConfig(level=logging.INFO, stream=sys.stdout)
asyncio.run(main())
